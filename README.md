# 连享会·五一论文班

&emsp;

## A. 答疑和交流

> - &#x1F449; [wiki - FAQs](https://gitee.com/lianxh/paper/wikis/FAQs-2022/B0-%E8%AF%BE%E5%89%8D%E9%A2%84%E4%B9%A0%E8%B5%84%E6%96%99.md)  
>   - 每天的课后答疑整理都会发布于此

&emsp;

## B. 课程概览
> **课程大纲：** [Click me](https://gitee.com/lianxh/paper/blob/master/outline.md)   
> **时间：** 2022 年 5 月 2-4 日 (9:00-12:00; 14:30-17:30, 半小时答疑)  
> **方式：** 网络直播+回放  
> **嘉宾：** 连玉君 (中山大学)；申广军(中山大学)； 杨海生(中山大学)  
> **课程主页：** <https://gitee.com/lianxh/paper> &#x2B55; [PDF课程大纲](https://file.lianxh.cn/KC/lianxh_paper.pdf)   
> **报名链接：** <http://junquan18903405450.mikecrm.com/6sFqyrI>

- 授课顺序
  - 连玉君 &ensp; 5 月 2 日
  - 申广军 &ensp; 5 月 3 日
  - 杨海生 &ensp; 5 月 4 日
- &#x26F3; [浏览所有论文](https://www.jianguoyun.com/p/DbU1jrIQtKiFCBi9v7ME)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

## &#x1F34F; 相关课程

&emsp;

## 1. 效率分析专题 - TFP、SFA、DEA
> ### &#x23E9; **2022 [效率分析专题](https://gitee.com/lianxh/TE)**   
> &#x231A; 2022.5.22 (周日)；5.28-29 (周六-周日)   
> &#x2B55; 龚斌磊 (浙江大学)；杜克锐 (厦门大学)  
> &#x26FA; **课程主页**：<https://gitee.com/lianxh/TE>，&#x2B55; [PDF课纲](https://file.lianxh.cn/KC/lianxh_TE.pdf)，&#x2B54; [-参考文献-](https://www.jianguoyun.com/p/DTOklyYQtKiFCBiK6bgEIAA)  
> &#x26FD; **报名链接：** <http://junquan18903405450.mikecrm.com/FhWLhS2>  
> &#x26BE; **助教招聘：** <https://www.wjx.top/vj/OtquLk5.aspx>

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh-TE-01.png)](https://gitee.com/lianxh/TE) 


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/底部图片_还上远帆001.png)

&emsp;

## 2. 视频专题 - 反复看

> ### &#x23E9; **邱嘉平教授：[因果推断实用计量方法](https://gitee.com/lianxh/YGqjp)**   
> &#x231A; 长期有效 (上架时间：2022.4.10)；软件：Stata   
> &#x2B55; [邱嘉平教授](https://profs.degroote.mcmaster.ca/ads/jiaping/)  (加拿大麦克玛斯特大学)  
> &#x26FE; **答疑服务：** 15 位助教提供全程答疑，[FAQs](https://gitee.com/lianxh/YGqjp/wikis/Home)    
> &#x26FA; **课程主页**：<https://gitee.com/lianxh/YGqjp>，&#x2B55; [PDF课纲](https://file.lianxh.cn/KC/lianxh_YGqjp.pdf)  
> &#x26FD; **报名链接：** <http://junquan18903405450.mikecrm.com/Yz6Djuv>
  - 长按/扫描二维码报名：    
   ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：因果推断使用计量方法.png)

### 授课嘉宾

**[邱嘉平](https://profs.degroote.mcmaster.ca/ads/jiaping/)**，加拿大麦克玛斯特大学德格罗特商学院金融系终身教授及加拿大帝国商业银行讲席教授 ([IDEAS](https://ideas.repec.org/e/pqi44.html), [NBER](https://www.nber.org/people/jiaping_qiu), [Google](https://xs2.dailyheadlines.cc/citations?user=meoIFeYAAAAJ&hl=zh-CN&oi=ao))；多项研究成果发表在 Journal of Financial Economics、Review of Financial Studies、Journal of Financial Quantitative and Analysis、Management Science、Accounting Review 等金融学、管理学和会计学国际顶级学术期刊上；论文获 Northern Finance Association Financial Management Association 年度最佳公司金融论文奖；担任 Frontier of Economics in China、Quarterly Review of Economics and Finance 以及 The International Journal of Accounting 期刊副主编；为瑞士国家基金、香港研究基金以及加拿大国家社会和人文科学基金评审员。

### 课程特色

> 本视频是邱嘉平老师《[因果推断实用计量方法](http://product.dangdang.com/28999209.html)》（2020，上海财经大学出版社）一书的配套视频。全套课程共 55 个视频，深入浅出地讲解面板数据模型、IV 估计、匹配方法、双重差分、自选择模型和断点回归等因果推断主流计量方法，辅以 Stata 实例。

因果关系实证方法是现代社会科学研究的基础。本书旨在为本科生、研究生和从事社科科学实证研究的科研工作者提供一个缩短计量经济学理论学习和实证研究间距离的桥梁。为达到这个目的，本书的内容坚持三个特点：

- **逻辑性**。以因果推断为核心，梳理各种方法的逻辑联系和优缺点。本书尽量避免不必要的数学证明，重点解读各种因果推断方法的原理和它们之间的联系及区别。
- **直观性**。尽量用很少的数学公式，通过直白的语言和例子来理解各种方法的本质。本书各章都会通过图形和简单的具体数值例子来直观解释不同的计量方法。
- **实用性**。本书涵盖了因果推断中很常用的方法，包括简单回归、匹配方法、面板分析、双重差分法、工具变量、样本自选择模型和断点回归，着重讲解不同计量方法在实际运用过程中将面对的各种细节问题。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_Qiu_YG-01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)






&emsp;



## 3. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[Bilibili 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[Bilibili 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 



### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/PX) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn) | [Bilibili](https://space.bilibili.com/546535876)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 
- &#x26F3; [连享会 · Bilibili 站](https://space.bilibili.com/546535876)

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1



&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;



&emsp;

## 4. 相关课程

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**    
- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[Bilibili 版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [Bilibili 版](https://space.bilibili.com/546535876/channel/detail?cid=160748)   
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)   


&emsp;

---
### 课程一览   


> 支持回看

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)**|   | 因果推断, 空间计量，寒暑假班等  |
| &#x2B55; **[数据清洗系列](https://gitee.com/arlionn/dataclean)** | 游万海| 直播, 88 元，已上线 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |



> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。



&emsp;


## 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等



> <font color=red>New！</font> **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


